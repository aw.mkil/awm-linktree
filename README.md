# AWM LinkTree
Simple link tree. Inspired by [this](https://github.com/MichaelBarney/LinkFree).

Self hosted [here](https://wadie.cool) using my home server.